package com.gabrielhuff.example.albums

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_albums.*

class AlbumListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_albums)

        val repo = Dependencies.albumRepository()
        albumListView.setAlbums(repo.getAlbums().toList())
        albumListView.onClicked = { id -> startAlbumDetail(id) }
    }
}

