package com.gabrielhuff.example.albums.repository

import com.gabrielhuff.example.albums.model.Album
import io.reactivex.Observable
import io.reactivex.Single

interface AlbumRepository {

    fun getAlbums(): Observable<Album>

    fun getAlbum(id: Long): Single<Album>
}