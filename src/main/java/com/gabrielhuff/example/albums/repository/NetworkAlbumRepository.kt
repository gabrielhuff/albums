package com.gabrielhuff.example.albums.repository

import com.gabrielhuff.example.albums.model.Album
import com.gabrielhuff.example.albums.model.Photo
import com.jakewharton.retrofit2.converter.kotlinx.serialization.stringBased
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class NetworkAlbumRepository : AlbumRepository {

    private val service = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(stringBased(MediaType.parse("application/json")!!, JSON.Companion::parse, JSON.Companion::stringify))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(Service::class.java)

    /**
     * Caches the result of [getAlbumPhotos] so we don't end up sending a bunch of requests
     */
    private val photosCache = mutableMapOf<Long, Observable<out Photo>>()

    override fun getAlbums(): Observable<Album> = service.getAlbums()
            .toObservable()
            .flatMapIterable { it }
            .map { Album(it.id, it.title, getAlbumPhotos(it.id)) }
            .cache()

    override fun getAlbum(id: Long): Single<Album> = service.getAlbum(id)
            .map { Album(it.id, it.title, getAlbumPhotos(it.id)) }
            .cache()

    private fun getAlbumPhotos(albumId: Long): Observable<out Photo> = photosCache.getOrPut(albumId) {
        service.getPhotos(albumId)
                .toObservable()
                .flatMapIterable { it }
                .map { Photo.Network(it.thumbnailUrl) }
                .cache()
    }

    interface Service {

        @GET("albums")
        fun getAlbums(): Single<List<NetworkAlbum>>

        @GET("albums/{id}")
        fun getAlbum(@Path("id") id: Long): Single<NetworkAlbum>

        @GET("photos")
        fun getPhotos(@Query("albumId") albumId: Long): Single<List<NetworkPhoto>>
    }

    @Serializable
    data class NetworkAlbum(val userId: Long, val id: Long, val title: String)

    @Serializable
    data class NetworkPhoto(val albumId: Long, val id: Long, val title: String, val url: String, val thumbnailUrl: String)
}