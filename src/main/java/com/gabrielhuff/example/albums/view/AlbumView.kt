package com.gabrielhuff.example.albums.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.AttrRes
import androidx.annotation.StyleRes
import com.gabrielhuff.example.albums.R
import com.gabrielhuff.example.albums.model.Album
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.album.view.*

/**
 * View that displays a single album. Composed by a title and a horizontal scrollable list of photos
 */
class AlbumView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        @AttrRes defStyleAttr: Int = 0,
        @StyleRes defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {

    private var photosDisposable: Disposable? = null

    init {
        View.inflate(context, R.layout.album, this)
        orientation = VERTICAL
    }

    fun setAlbum(album: Album) {
        titleView.text = album.title

        photosDisposable?.dispose()

        photosDisposable = album.photos.toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { photoListView.setPhotos(emptyList()) }
                .doOnSuccess { photoListView.setPhotos(it) }
                .toCompletable()
                .onErrorComplete()
                .subscribe()
    }
}