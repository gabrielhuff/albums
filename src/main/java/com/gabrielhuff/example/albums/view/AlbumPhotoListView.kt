package com.gabrielhuff.example.albums.view

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gabrielhuff.example.albums.model.Photo

/**
 * View that displays a list of photos on an scrollable horizontal list
 */
class AlbumPhotoListView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0
) : RecyclerView(context, attrs, defStyle) {

    private val photosAdapter = PhotosAdapter()

    init {
        layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        adapter = photosAdapter
        isHorizontalFadingEdgeEnabled = true
        setFadingEdgeLength(FADING_EDGE_DP.dpToPixel)
    }

    fun setPhotos(photos: List<Photo>) { photosAdapter.submitList(photos) }


    private val Int.dpToPixel: Int get() =
            (context.resources.displayMetrics.density * this).toInt()

    private inner class PhotosAdapter : ListAdapter<Photo, PhotoViewHolder>(PhotoDiffUtil()) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
            val size = IMAGE_SIZE_DP.dpToPixel
            val lp = RecyclerView.LayoutParams(size, size)
            val margin = PHOTO_MARGIN_DP.dpToPixel
            lp.setMargins(margin, margin, margin, margin)
            val view = AlbumPhotoView(context).apply { layoutParams = lp }
            return PhotoViewHolder(view)
        }

        override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
            holder.view.setPhoto(getItem(position))
        }
    }

    private inner class PhotoViewHolder(val view: AlbumPhotoView): ViewHolder(view)

    private inner class PhotoDiffUtil : DiffUtil.ItemCallback<Photo>() {

        override fun areItemsTheSame(old: Photo, new: Photo): Boolean = old === new

        override fun areContentsTheSame(old: Photo, new: Photo): Boolean = old == new
    }

    private companion object {

        const val IMAGE_SIZE_DP = 92

        const val FADING_EDGE_DP = 16

        const val PHOTO_MARGIN_DP = 8
    }
}