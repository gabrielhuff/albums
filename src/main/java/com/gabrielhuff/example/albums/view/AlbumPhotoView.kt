package com.gabrielhuff.example.albums.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import androidx.annotation.AttrRes
import androidx.annotation.StyleRes
import com.bumptech.glide.Glide
import com.gabrielhuff.example.albums.R
import com.gabrielhuff.example.albums.model.Photo

/**
 * View that displays an individual album photo
 */
class AlbumPhotoView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        @AttrRes defStyleAttr: Int = 0,
        @StyleRes defStyleRes: Int = 0
) : ImageView(context, attrs, defStyleAttr, defStyleRes) {


    init {
        clipToOutline = true
        setBackgroundResource(R.drawable.album_photo_mask)
    }

    fun setPhoto(photo: Photo?) {
        when (photo) {
            is Photo.Network -> Glide.with(context).load(photo.url).into(this)
            is Photo.Resource -> setImageResource(photo.res)
            null -> setImageBitmap(null)
        }
    }
}