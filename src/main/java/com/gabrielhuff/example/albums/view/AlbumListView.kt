package com.gabrielhuff.example.albums.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gabrielhuff.example.albums.R
import com.gabrielhuff.example.albums.model.Album
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.album_list.view.*

/**
 * View that displays a list of albums on a vertical list. Each album has a horizontal list of photos
 */
class AlbumListView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {

    private val albumsAdapter = AlbumsAdapter()

    private var albumsDisposable: Disposable? = null

    var onClicked: ((albumId: Long) -> Unit)? = null

    init {
        View.inflate(context, R.layout.album_list, this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = albumsAdapter
    }

    fun setAlbums(albums: Single<List<Album>>) {
        albumsDisposable?.dispose()

        albumsDisposable = albums
                .subscribeOn(Schedulers.io())
                .map { it.sortedBy(Album::title) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    albumsAdapter.submitList(emptyList())
                    recyclerView.visibility = View.INVISIBLE
                    progressBar.visibility = View.VISIBLE
                    errorView.visibility = View.INVISIBLE
                }
                .doOnSuccess {
                    albumsAdapter.submitList(it)
                    recyclerView.visibility = if (it.isEmpty()) View.INVISIBLE else View.VISIBLE
                    progressBar.visibility = View.INVISIBLE
                    errorView.visibility = if (it.isEmpty()) View.VISIBLE else View.INVISIBLE
                    errorView.text = context.getString(R.string.no_albums_found)
                }
                .doOnError {
                    recyclerView.visibility = View.INVISIBLE
                    progressBar.visibility = View.INVISIBLE
                    errorView.visibility = View.VISIBLE
                    errorView.text = context.getString(R.string.couldnt_get_albums_check_your_internet_connection)
                }
                .toCompletable()
                .onErrorComplete()
                .subscribe()
    }

    private inner class AlbumsAdapter : ListAdapter<Album, AlbumViewHolder>(AlbumDiffUtil()) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
            val lp = RecyclerView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )
            val view = AlbumView(context).apply { layoutParams = lp }
            return AlbumViewHolder(view)
        }

        override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
            val item = getItem(position)
            holder.view.setAlbum(item)
            holder.view.setOnClickListener { onClicked?.invoke(item.id) }
        }
    }

    private inner class AlbumViewHolder(val view: AlbumView): RecyclerView.ViewHolder(view)

    private inner class AlbumDiffUtil : DiffUtil.ItemCallback<Album>() {

        override fun areItemsTheSame(old: Album, new: Album): Boolean = old === new

        override fun areContentsTheSame(old: Album, new: Album): Boolean = false
    }
}