package com.gabrielhuff.example.albums.model

import androidx.annotation.DrawableRes

sealed class Photo {

    data class Network(val url: String) : Photo()

    data class Resource(@DrawableRes val res: Int) : Photo()
}