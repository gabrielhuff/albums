package com.gabrielhuff.example.albums.model

import io.reactivex.Observable

data class Album(val id: Long, val title: String, val photos: Observable<out Photo>)