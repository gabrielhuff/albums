package com.gabrielhuff.example.albums

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gabrielhuff.example.albums.view.AlbumView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AlbumDetailActivity : AppCompatActivity() {

    private val id by lazy { intent.getLongExtra(ARG_ID, 0) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val view = AlbumView(this)
        setContentView(view)

        val repo = Dependencies.albumRepository()
        repo.getAlbum(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { album -> view.setAlbum(album) }
    }

    companion object {

        const val ARG_ID = "id"
    }
}

fun Activity.startAlbumDetail(id: Long) {
    val intent = Intent(this, AlbumDetailActivity::class.java).apply {
        putExtra(AlbumDetailActivity.ARG_ID, id)
    }
    startActivity(intent)
}