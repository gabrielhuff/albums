package com.gabrielhuff.example.albums

import com.gabrielhuff.example.albums.repository.AlbumRepository
import com.gabrielhuff.example.albums.repository.NetworkAlbumRepository

/**
 * This is a lightweight dependency injection mechanism. Override the dependency functions
 * during tests so that activities receive the overridden instances.
 */
object Dependencies {

    var albumRepository: () -> AlbumRepository = { NetworkAlbumRepository() }
}