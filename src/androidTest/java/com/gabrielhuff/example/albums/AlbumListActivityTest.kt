package com.gabrielhuff.example.albums

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.gabrielhuff.example.albums.model.Album
import com.gabrielhuff.example.albums.model.Photo
import com.gabrielhuff.example.albums.repository.AlbumRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.util.concurrent.TimeUnit

class AlbumListActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(AlbumListActivity::class.java, false, false)

    @Test
    fun loadAlbumsSuccessfully() {
        // GIVEN that repo contains 3 albums: cats, dogs and alpacas
        AlbumListActivity.Dependencies.albumRepository = { SuccessAlbumRepository() }

        // WHEN activity is started
        activityRule.launchActivity(null)

        // THEN albums are being loaded
        onView(withId(R.id.progressBar)).check(matches(isDisplayed()))

        // AND all albums are shown after load is successful
        Thread.sleep(6000) // Sleep 6 seconds as mocked loading time is 4 seconds
        onView(withText("Cats")).check(matches(isDisplayed()))
        onView(withText("Dogs")).check(matches(isDisplayed()))
        onView(withText("Alpacas")).check(matches(isDisplayed()))
    }

    @Test
    fun loadAlbumsWithError() {
        // GIVEN that repo will fail to load albums
        AlbumListActivity.Dependencies.albumRepository = { ErrorAlbumRepository() }

        // WHEN activity is started
        activityRule.launchActivity(null)

        // THEN albums are being loaded
        onView(withId(R.id.progressBar)).check(matches(isDisplayed()))

        // AND an error message is displayed after load fails
        Thread.sleep(6000) // Sleep 6 seconds as mocked loading time is 4 seconds
        onView(withId(R.id.errorView)).check(matches(isDisplayed()))
    }

    class SuccessAlbumRepository : AlbumRepository {

        override fun getAlbums(): Observable<Album> = Observable.just(
                Album(
                        title = "Cats",
                        photos = Observable.just(
                                Photo.Resource(R.drawable.cat1),
                                Photo.Resource(R.drawable.cat2),
                                Photo.Resource(R.drawable.cat3)
                        )
                ),
                Album(
                        title = "Dogs",
                        photos = Observable.just(
                                Photo.Resource(R.drawable.dog1),
                                Photo.Resource(R.drawable.dog2),
                                Photo.Resource(R.drawable.dog3),
                                Photo.Resource(R.drawable.dog4)
                        )
                ),
                Album(
                        title = "Alpacas",
                        photos = Observable.just(
                                Photo.Resource(R.drawable.alpaca1),
                                Photo.Resource(R.drawable.alpaca2)
                        )
                )
        ).delay4SecondsAndObserveOnMainThread()
    }

    class ErrorAlbumRepository : AlbumRepository {

        override fun getAlbums(): Observable<Album> = Observable.empty<Album>()
                .delay4SecondsAndObserveOnMainThread()
                .concatWith(Observable.error(IOException()))
    }

    private companion object {

        private fun <T> Observable<T>.delay4SecondsAndObserveOnMainThread() = delay(4, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
